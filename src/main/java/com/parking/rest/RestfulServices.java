package com.parking.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.parking.entity.Record;
import com.parking.entity.User;
import com.parking.entity.Zone;
import com.parking.handler.Handler;
import com.parking.rest.util.RestUtil;

@Path("/")
public class RestfulServices {

	private Handler handler = new Handler();

	@Path("addemployee/{user}/{sessiondId}")
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addEmployee(User user, @PathParam("user") String userName, @PathParam("sessionId") String sessionId)
			throws JSONException, URISyntaxException {
		boolean session = RestUtil.checkSession(userName, sessionId);
		if (!session)
			return Response.status(302).location(new URI("http://localhost:8080/parking/public/views/login.html")).build();
		JSONObject jsonObject = handler.createUser(user);
		if (jsonObject == null) {
			return Response.status(403).build();
		}
		return Response.status(200).entity(jsonObject.toString()).build();
	}

	@Path("changeemployeezone/{user}/{sessionId}")
	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response addEmployeeToZone(String json, @PathParam("user") String userName,
			@PathParam("sessionId") String sessionId) throws JSONException, URISyntaxException {
		boolean session = RestUtil.checkSession(userName, sessionId);
		if (!session)
			return Response.status(302).location(new URI("http://localhost:8080/parking/public/views/login.html")).build();
		JSONObject jsone = new JSONObject(json);
		boolean ok = handler.changeEmployeeZone(jsone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("addzone/{user}/{sessionId}")
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response addZone(Zone zone, @PathParam("user") String userName, @PathParam("sessionId") String sessionId)
			throws JSONException, URISyntaxException {
		boolean session = RestUtil.checkSession(userName, sessionId);
		if (!session)
			return Response.status(302).location(new URI("http://localhost:8080/parking/public/views/login.html")).build();
		boolean ok = handler.addNewZone(zone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("createzone")
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createZone(Zone zone) {
		boolean ok = handler.createNewZone(zone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("addcoordstozone")
	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response addCoordsToZone(String json) throws JSONException {
		JSONObject jsone = new JSONObject(json);
		boolean ok = handler.editZone(jsone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("deletezone")
	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response deleteZone(String json) throws JSONException {
		JSONObject jsone = new JSONObject(json);
		boolean ok = handler.deleteZone(jsone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("record")
	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response record(Record rec) {
		int ok = handler.record(rec);
		if (ok == RestUtil.PAYMENT_SUCCESSFUL) {
			return Response.status(200).build();
		} else if (ok == RestUtil.ALREADY_PAID) {
			return Response.status(403).build();
		}
		return Response.status(503).build(); // payment failed
	}

	@Path("deleteuser")
	@POST
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response deleteUser(String json) throws JSONException {
		JSONObject jsone = new JSONObject(json);
		boolean ok = handler.deleteUser(jsone);
		if (ok)
			return Response.status(200).build();
		return Response.status(403).build();
	}

	@Path("getallpolygons")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public String getAllPolygons() {
		JSONArray json = null;
		json = handler.fetchAllPolygons();
		return json.toString();
	}

	@Path("getzone")
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public String getZone(String json) throws JSONException {
		JSONObject jsone = new JSONObject(json);
		JSONObject responseJSON = handler.getZone(jsone);
		return responseJSON.toString();
	}

	@Path("login")
	@POST
	public Response login(String json) throws JSONException, URISyntaxException {
		Object[] response = handler.tryLogin(new JSONObject(json));
		if ((int) response[0] == RestUtil.LOGGED_IN) {
			if (response[2] != null) {
				if (((User) response[2]).getPosition().equals("MANAGER")) {
					URI targetURIForRedirection = new URI("http://localhost:8080/parking/public/views/manager.html");
					NewCookie cookies[] = new NewCookie[2];
					cookies[0] = ((NewCookie[]) response[1])[0];
					cookies[1] = ((NewCookie[]) response[1])[1];
					return Response.status(302).location(targetURIForRedirection).cookie(cookies).build();
				} else if (((User) response[2]).getPosition().equals("EMPLOYEE")) {
					URI targetURIForRedirection = new URI("http://localhost:8080/parking/public/views/employee.html");
					NewCookie cookies[] = new NewCookie[2];
					cookies[0] = ((NewCookie[]) response[1])[0];
					cookies[1] = ((NewCookie[]) response[1])[1];
					return Response.status(302).location(targetURIForRedirection).cookie(cookies).build();
				}
			}
		}
		return Response.status(403).build();
	}

	@Path("checkplate")
	@GET
	public Response checkPlate(@QueryParam(value = "plateNumber") String plateNumber,
			@QueryParam(value = "zoneId") int zoneId) {
		JSONArray jsonArray = handler.checkPlate(plateNumber, zoneId);
		if (jsonArray != null) {
			return Response.ok(jsonArray.toString(), MediaType.APPLICATION_JSON).status(200).build();
		}
		return Response.ok().status(401).build();
	}

	@Path("statistics/{user}/{sessionId}")
	@POST
	public Response getStatistics(String json, @PathParam("user") String userName,
			@PathParam("sessionId") String sessionId) throws JSONException, URISyntaxException {
		boolean session = RestUtil.checkSession(userName, sessionId);
		if (!session)
			return Response.status(302).location(new URI("http://localhost:8080/parking/public/views/login.html")).build();
		JSONObject jsone = new JSONObject(json);
		JSONObject statisticBean = handler.getStatistics(jsone);
		return Response.status(200).entity(statisticBean.toString()).build();
	}

	// Dead code used for test
	@Path("redirect")
	@GET
	public Response resp() throws URISyntaxException {
		URI targetURIForRedirection = new URI("http://data.bg");
		NewCookie cookies[] = new NewCookie[1];
		NewCookie cookie = new NewCookie("pesho", "peshkata");
		cookies[0] = cookie;
		return Response.status(302).location(targetURIForRedirection).cookie(cookies).build();
	}

}
