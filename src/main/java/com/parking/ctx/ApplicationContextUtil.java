package com.parking.ctx;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ApplicationContextUtil implements ApplicationContextAware {
	private static ApplicationContext ctx = null;

	public static ApplicationContext getApplicationContext() {
		return ctx;
	}

	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;
	}
	
	public static Object get(String bean) {
		return ctx.getBean(bean);
	}

	@SuppressWarnings("unused")
	private void destroyCtx() {
		((ClassPathXmlApplicationContext) ctx).close();
	}
}
