package com.parking.entity;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.joda.time.DateTime;


@Entity
@Table(name ="records")
@XmlRootElement
public class Record {
	
	@Id
	@Column(name="recordid")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long recordid;
	
	@Column(name="datefrom")
	private Date datefrom = new Date();
	
	@Column(name="dateto")
	private Date dateto = null;
	
	@Column(name="paid")
	private long paid;
	
	@Transient
	private int time = -1; // hours paid for
	
	@XmlElement
	@Column(name="platenumber")
	private String platenumber;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="zoneid")
	@XmlElement (name="zone")
	private Zone zone;
	
	@XmlElement(name="time")
	public void setTime(int time){
		DateTime dateTime = new DateTime(datefrom);
		dateTime = dateTime.plusHours(time);
		this.time = time; //magic
		this.dateto = dateTime.toDate();
	}
	
	@Column(name="expired")
	private boolean expired = false;
	
	public void setDateTo(Date date){
		this.dateto = date;
	}
	
	public void setDateFrom(Date date){
		this.datefrom = date;
	}
	
	public int getTime(){
		return time;
	}

	public long getRecordId() {
		return recordid;
	}

	public void setRecordId(long recordid) {
		this.recordid = recordid;
	}

	public Date getDateFrom() {
		return datefrom;
	}

	public Date getDateTo() {
		return dateto;
	}

	public long getPaid() {
		return paid;
	}

	public void setPaid(long paid) {
		this.paid = paid;
	}

	public String getPlateNumber() {
		return platenumber;
	}
	
	public boolean isExpired(){
		return expired;
	}
	
	public void expire(){
		this.expired = true;
	}

	public void setPlateNumber(String platenumber) {
		this.platenumber = platenumber;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone=zone;
	}
	
	@Override
	public boolean equals(Object rec){
		if(rec instanceof Record){
			Record right = (Record) rec;
			if(right.getRecordId() == recordid){
				return true;
			}
			return false;
		}
		return false;
	}
	
	@Override 
	public int hashCode(){
		return (int) ((recordid >> 32) ^ recordid);
	}

}
