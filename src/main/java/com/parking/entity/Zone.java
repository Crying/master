package com.parking.entity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "zones")
@XmlRootElement
public class Zone {

	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "zoneemployee", joinColumns = @JoinColumn(name = "zoneid"), inverseJoinColumns = @JoinColumn(name = "userid"))
	private Set<User> users = new HashSet<User>();

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "zonecoords", joinColumns = @JoinColumn(name = "zoneid"))
	@OrderBy("zoneid")
	@XmlElement (name="points")
	private Set<Point> points = new LinkedHashSet<Point>();

	public Set<Point> getCoords() {
		return points;
	}

	@Id
	@Column(name = "zoneid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@XmlElement(name = "zoneId")
	private long zoneId;

	@XmlElement
	private String zoneName;

	@Column(name = "removed")
	private boolean removed = false;

	@Column(name = "occupied")
	private int occupied = 0;

	@Column(name = "price")
	@XmlElement(name = "price")
	private int price;

	public int getPrice() {
		return price;
	}

	public boolean tryOccupy() {
		if (occupied + 1 > capacity) {
			return false;
		} else {
			occupied++;
			return true;
		}
	}

	public int getOccupied() {
		return occupied;
	}

	public int getCapacity() {
		return capacity;
	}

	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	@XmlElement
	@Column(name = "capacity")
	private int capacity;

	public long getZoneId() {
		return zoneId;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void addUser(User user) {
		users.add(user);
	}

	public void addCoords(Point p) {
		points.add(p);
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public void removeUsers(Zone z) {
		Iterator<User> usr = users.iterator();
		while (usr.hasNext()) {
			User u = usr.next();
			u.setZone(null);
		}
	}

	public synchronized void decrementOccupied() {
		if (occupied > 0) {
			occupied--;
		}
	}

}
