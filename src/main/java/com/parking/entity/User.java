package com.parking.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "users")
@XmlRootElement
public class User {

	public static String employee = "EMPLOYEE";
	public static String manager = "MANAGER";

	@Override
	public boolean equals(Object o) {
		if (o != null) {
			if ((o instanceof User) && (((User) o).getId() == this.userid)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	@Override 
	public int hashCode(){
		return (int) ((userid >> 32) ^ userid);
	}

	private long getId() {
		return userid;
	}

	@Id
	@Column(name = "userid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userid;

	@Column(name = "name")
	@XmlElement
	private String name;

	@Column(name = "username")
	@XmlElement
	private String username;

	@Column(name = "password")
	@XmlElement
	private String password;

	@Column(name = "position")
	@XmlElement
	private String userType;

	@Column(name = "hash")
	private String hash;

	@XmlElement(name = "zone")
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "zoneid")
	private Zone zone;

	@Column(name = "sessionid")
	private String sessionID;

	@Column(name = "sessionexpiry")
	private Date sessionExpiry;

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public Zone getZone() {
		return zone;
	}

	public User() {
		// make jaxb happy
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPosition() {
		return userType;
	}

	public void setPosition(String userType) {
		this.userType = userType;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public void setSessionExpiry(Date date) {
		this.sessionExpiry = date;
	}

	public Date getSessionExpiry() {
		return sessionExpiry;
	}

	public String getSessionID() {
		return sessionID;
	}

}
