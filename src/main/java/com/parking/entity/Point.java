package com.parking.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Embeddable
@Table(name="zonecoords")
@XmlRootElement
public class Point {
	
	@XmlElement(name = "lon")
	@Column(name ="pointlon")
	private String longitude;
	
    @XmlElement (name ="lat")
    @Column(name="pointlat")
	private String latitude;
	
    public Point(){
    	
    }
    
	public Point(Object object, Object object2) {
		this.latitude = (String) object;
		this.longitude = (String) object2;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	
}
