package com.parking.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.parking.ctx.ApplicationContextUtil;
import com.parking.entity.Point;
import com.parking.entity.Record;
import com.parking.entity.User;
import com.parking.entity.Zone;
import com.parking.log.LogUtil;
import com.parking.rest.util.RestUtil;

public class Handler {

	public synchronized JSONObject createUser(User user) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String HQL = "FROM com.parking.entity.User WHERE username = :username";
				Query q = s.createQuery(HQL).setParameter("username", user.getUsername());
				ArrayList<User> resultSet = (ArrayList<User>) q.list();
				if (resultSet.size() == 0) {
					String pass = RestUtil.handlePassword(user);
					HQL = "FROM com.parking.entity.Zone WHERE zoneid = :zoneId";
					if (user.getZone() != null) {
						Query qe = s.createQuery(HQL).setParameter("zoneId", user.getZone().getZoneId());
						Zone zone = (Zone) qe.uniqueResult();
						if (zone != null) {
							zone.addUser(user);
						}
						user.setZone(zone);
					}
					s.saveOrUpdate(user);
					return RestUtil.constructJSONUsernamePassword(user, pass);
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return null;
			} finally {
				tx.commit();
				s.clear();
			}
		}
		return null;
	}

	public synchronized boolean changeEmployeeZone(JSONObject json) throws JSONException {
		long zoneId = json.getInt("zoneId");
		String employeeName = (String) json.getString("employeeName");
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				Zone zone = s.get(Zone.class, zoneId);
				if (zone != null) {
					String HQL2 = "FROM com.parking.entity.User WHERE username = :employeeName";
					Query query2 = s.createQuery(HQL2).setParameter("employeeName", employeeName);
					User user = (User) query2.uniqueResult();
					if (user != null) {
						user.setZone(zone);
						zone.addUser(user);
						return true;
					} else {
						return false;
					}
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return false;
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized boolean addNewZone(Zone zone) throws JSONException {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				s.saveOrUpdate(zone);
				return true;
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return false;
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized boolean editZone(JSONObject jsone) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String zoneId = jsone.getString("zoneId");
				String zoneNewName = null;
				if (jsone.has("zoneNewName")) {
					zoneNewName = jsone.getString("zoneNewName");
				}
				JSONObject p = jsone.getJSONObject("points");
				JSONArray lon = p.getJSONArray("lon");
				JSONArray lat = p.getJSONArray("lat");
				if (p == null || lat == null || lon == null)
					return false;
				if (lat.length() != lon.length()) {
					throw new Exception("Length of the lon and lat arrays should be the same.");
				}
				String HQL = "FROM com.parking.entity.Zone WHERE zoneid = :zoneId";
				Query q = s.createQuery(HQL).setParameter("zoneId", zoneId);
				ArrayList<Zone> zones = (ArrayList<Zone>) q.list();
				if (zones.size() == 1) {
					Zone z = zones.get(0);
					if (zoneNewName != null) {
						z.setZoneName(zoneNewName);
					}
					for (int i = 0; i < lon.length(); i++) {
						z.addCoords(new Point(lat.get(i), lon.get(i)));
					}
					s.saveOrUpdate(z);
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return false;
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized boolean deleteUser(JSONObject jsone) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String username = jsone.getString("username");
				if (username == null) {
					return false;
				}
				String HQL = "FROM com.parking.entity.User WHERE username =  :username";
				Query q = s.createQuery(HQL).setParameter("username", username);
				ArrayList<User> users = (ArrayList<User>) q.list();
				if (users.size() == 1) {
					User u = users.get(0);
					s.delete(u);
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return false;
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized boolean deleteZone(JSONObject jsone) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String zoneId = jsone.getString("zoneId");
				Zone zone = (Zone) s.get(Zone.class, Long.valueOf(zoneId));
				if (zone.getOccupied() == 0) {
					s.delete(zone);
					return true;
				} else {
					return false;
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
				return false;
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized int record(Record rec) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String plateNumber = rec.getPlateNumber();
				if (plateNumber == null) {
					return RestUtil.PAYMENT_FAILED;
				}
				if (plateNumber.isEmpty()) {
					return RestUtil.PAYMENT_FAILED;
				}
				String HQL = "FROM com.parking.entity.Record as rec WHERE rec.platenumber = "
						+ ":plateNumber AND rec.dateto >= current_timestamp() AND expired=false order by dateTo desc";
				Query query = s.createQuery(HQL).setParameter("plateNumber", plateNumber);
				ArrayList<Record> records = (ArrayList<Record>) query.list();
				if (records.size() == 0) {
					long zoneId = rec.getZone().getZoneId();
					Zone zone = s.get(Zone.class, zoneId);
					if (zone != null) {
						if (!zone.isRemoved()) {
							if (zone.tryOccupy()) {
								rec.setPaid(rec.getTime() * zone.getPrice());
								rec.setZone(zone);
								s.saveOrUpdate(rec);
								return RestUtil.PAYMENT_SUCCESSFUL;
							}
						}
					}
				} else {
					// case pay for more time
					Record record = records.get(0);
					rec.setDateFrom(record.getDateTo());
					DateTime dateTime = new DateTime(record.getDateTo());
					dateTime = dateTime.plusHours(rec.getTime());
					rec.setDateTo(dateTime.toDate());
					rec.setPaid(rec.getTime() * record.getZone().getPrice());
					s.save(rec);
					return RestUtil.PAYMENT_SUCCESSFUL;
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
			} finally {
				tx.commit();
				s.close();
			}
		}
		return RestUtil.PAYMENT_FAILED;
	}

	public JSONArray fetchAllPolygons() {
		LogUtil.info("Fetching all Polygons.");
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Session s = null;
		JSONArray jsonArray = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				String HQL = "FROM com.parking.entity.Zone WHERE removed = false";
				Query query = s.createQuery(HQL);
				ArrayList<Zone> zones = (ArrayList<Zone>) query.list();
				if (zones != null) {
					jsonArray = new JSONArray();
					for (int i = 0; i < zones.size(); i++) {
						Set<Point> currZonePoints = zones.get(i).getCoords();
						JSONObject jsonObject = new JSONObject();
						jsonObject.put("zoneId", zones.get(i).getZoneId());
						jsonObject.put("zoneName", zones.get(i).getZoneName());
						String[] lon = new String[currZonePoints.size()];
						String[] lat = new String[currZonePoints.size()];
						int wu = 0;
						Iterator<Point> iter = currZonePoints.iterator();
						while (iter.hasNext()) {
							Point point = iter.next();
							lon[wu] = point.getLongitude();
							lat[wu] = point.getLatitude();
							wu++;
						}
						JSONObject jsonTemp = new JSONObject();
						jsonTemp.put("lat", Arrays.toString(lat));
						jsonTemp.put("lon", Arrays.toString(lon));
						jsonObject.put("points", jsonTemp);
						jsonArray.put(jsonObject);
					}
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
			} finally {
				s.close();
			}
		}
		return jsonArray;
	}

	public JSONObject getZone(JSONObject jsone) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Session s = null;
		JSONObject jsonResponse = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				String zoneId = jsone.getString("zoneId");
				if (zoneId != null) {
					Zone zone = (Zone) s.get(Zone.class, Long.valueOf(zoneId));
					if (zone != null) {
						jsonResponse = new JSONObject();
						jsonResponse.put("zoneName", zone.getZoneName());
						jsonResponse.put("zoneId", zone.getZoneId());
						jsonResponse.put("zoneCapacity", zone.getCapacity());
						jsonResponse.put("zoneOccupied", zone.getOccupied());
						jsonResponse.put("zonePrice", zone.getPrice());
					} else {
						throw new Exception("No such zoneId: " + zoneId);
					}
				} else {
					throw new Exception("Zone Id is null");
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
			} finally {
				s.close();
			}
		}
		return jsonResponse;
	}

	public synchronized boolean createNewZone(Zone zone) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				s.saveOrUpdate(zone);
				return true;
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
			} finally {
				tx.commit();
				s.close();
			}
		}
		return false;
	}

	public synchronized Object[] tryLogin(JSONObject jsonObject) throws JSONException {
		NewCookie cookies[] = null;
		String passWord = jsonObject.getString("password");
		String userName = jsonObject.getString("username");
		if (userName == null || passWord == null) {
			return new Object[] { RestUtil.INCORRECT_CREDENTIALS, null, null };
		}
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Transaction tx = null;
		Session s = null;
		if (sf != null) {
			try {
				s = sf.openSession();
				tx = s.beginTransaction();
				String HQL = "FROM com.parking.entity.User WHERE username = :username";
				Query query = s.createQuery(HQL).setParameter("username", userName);
				User user = (User) query.uniqueResult();
				if (user == null) {
					return new Object[] { RestUtil.INCORRECT_CREDENTIALS, null, null };
				}
				// now try login
				String sessionID = RestUtil.tryLogin(user, passWord);
				if (sessionID != null) {
					user.setSessionID(sessionID);
					DateTime dateTime = new DateTime();
					dateTime = dateTime.plusHours(1);
					user.setSessionExpiry(dateTime.toDate());
					cookies = new NewCookie[2];
					cookies[0] = new NewCookie(new Cookie("user-SessionID", user.getSessionID(), "/", "localhost"),
							"Holds sessionID", (int) (Math.abs(user.getSessionExpiry().getTime() - System.nanoTime())),
							false);
					cookies[1] = new NewCookie(new Cookie("user-userName", user.getUsername(), "/", "localhost"),
							"Holds userName", (int) (Math.abs(user.getSessionExpiry().getTime() - System.nanoTime())),
							false);
					return new Object[] { RestUtil.LOGGED_IN, cookies, user };
				} else {
					return new Object[] { RestUtil.INCORRECT_CREDENTIALS, null, null };
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
				tx.rollback();
			} finally {
				tx.commit();
				s.close();
			}
		}
		return new Object[] { RestUtil.INCORRECT_CREDENTIALS, null, null };
	}

	public JSONArray checkPlate(String plateNumber, int zoneId) {
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Session s = null;
		JSONArray jsonArr = new JSONArray();
		if (sf != null) {
			try {
				s = sf.openSession();
				String HQL = "FROM com.parking.entity.Record "
						+ "WHERE platenumber = :plateNumber AND expired = false AND zoneId = :zoneId";
				Query query = s.createQuery(HQL).setParameter("plateNumber", plateNumber).setParameter("zoneId",
						zoneId);
				ArrayList<Record> records = (ArrayList<Record>) query.list();
				if (records.size() > 0) {
					records.parallelStream()
							.sorted((record1, record2) -> record1.getDateFrom().compareTo(record2.getDateFrom()))
							.forEachOrdered((record) -> {
								JSONObject json = new JSONObject();
								try {
									DateTimeFormatter fmt = DateTimeFormat.forPattern("dd MMMM, yyyy HH:mm");
									DateTime dateFrom = new DateTime(record.getDateFrom());
									DateTime dateTo = new DateTime(record.getDateTo());
									json.put("recordId", record.getRecordId());
									json.put("recordFrom", dateFrom.toString(fmt));
									json.put("recordTo", dateTo.toString(fmt));
								} catch (Exception e) {
									e.printStackTrace();
								}
								jsonArr.put(json);
							});
					return jsonArr;
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
			} finally {
				s.close();
			}
		}
		return null;
	}

	public JSONObject getStatistics(JSONObject jsone) throws JSONException {
		int zoneId = jsone.getInt("zoneId");
		DateTime startDateTemp = new DateTime(jsone.getString("startDate")); // use
																				// joda
																				// time
																				// because
																				// java.util.Date
																				// is
																				// pathetic
																				// in
																				// date
																				// string
																				// parsing
		DateTime endDateTemp = new DateTime(jsone.getString("endDate")); // use
																			// joda
																			// time
																			// because
																			// java.util.Date
																			// is
																			// pathetic
																			// in
																			// date
																			// string
																			// parsing
		SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
		Session s = null;
		JSONObject json = new JSONObject();
		if (sf != null) {
			try {
				s = sf.openSession();
				String HQL = "FROM com.parking.entity.Record " + "WHERE datefrom >= :startDate"
						+ " AND dateto <= :endDate" + " AND zoneId = :zoneId" + " AND expired = true";
				Query query = s.createQuery(HQL);
				query.setParameter("zoneId", zoneId);
				query.setParameter("startDate", startDateTemp.toDate());
				query.setParameter("endDate", endDateTemp.toDate());
				ArrayList<Record> records = (ArrayList<Record>) query.list();
				if (records.size() > 0) {
					json.put("totalParked", records.size());
					long totalValue = records.parallelStream().mapToLong((r) -> r.getPaid()).sum();
					json.put("totalValue", totalValue);
				}else{
					json.put("totalParked", 0);
					json.put("totalValue", 0);
				}
			} catch (Exception e) {
				LogUtil.error(LogUtil.stackTraceToObject(e), e);
			} finally {
				s.close();
			}
		}
		return json;
	}

}
