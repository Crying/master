package com.parking.log;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LogUtil {

	public static Logger log = Logger.getRootLogger();

	// Creation & retrieval methods:

	// printing methods:
	public static void trace(Object message, Throwable t) {
		log.trace(message);
	}

	public static void debug(Object message, Throwable t) {
		log.debug(message);
	}

	public static void info(Object message) {
		log.info(message);
	}

	public static void warn(Object message) {
		log.warn(message);
	}

	public static void error(Object message, Throwable t) {
		log.error(message);
	}

	public static void fatal(Object message, Throwable t) {
		log.fatal(message);
	}

	// generic printing method:
	public static void log(Level l, Object message) {
		log.log(l, message);
	}

	public static Object stackTraceToObject(Throwable t) {
		StringWriter sWriter = new StringWriter();
		PrintWriter pWriter = new PrintWriter(sWriter);
		t.printStackTrace(pWriter);
		return sWriter.toString();
	}

}
