package com.parking.crawler;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.parking.ctx.ApplicationContextUtil;
import com.parking.entity.Record;
import com.parking.log.LogUtil;

public class RefreshZone implements Runnable {

	private final int TIMEOUT = 60 * 1000;
	private boolean isStarted = true;

	public boolean isStarted() {
		return isStarted;
	}

	@Override
	public void run() {
		while (isStarted) {
			synchronized (this) {	
				try {
					this.wait(TIMEOUT);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
				// decrement zones
				SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
				Transaction tx = null;
				Session s = null;
				if (sf != null) {
					try {
						s = sf.openSession();
						tx = s.beginTransaction();
						String HQL = "FROM com.parking.entity.Record WHERE expired = false"
								+ " AND current_timestamp() >= dateto";
						Query q = s.createQuery(HQL);
						ArrayList<Record> records = (ArrayList<Record>) q.list();
						records.forEach((r) -> {
								r.getZone().decrementOccupied();
								r.expire();
						});
					} catch (Exception e) {
						LogUtil.error(LogUtil.stackTraceToObject(e), e);
						tx.rollback();
					} finally {
						tx.commit();
						s.close();
					}
				}
			}
		}
	}

}
