package com.parking.crawler;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.parking.ctx.ApplicationContextUtil;
import com.parking.entity.User;
import com.parking.log.LogUtil;

public class InvalidateSession implements Runnable{

	private final int TIMEOUT = 60 * 1000;
	private boolean isStarted = true;

	public boolean isStarted() {
		return isStarted;
	}

	public void run() {
		while (isStarted) {
			synchronized (this) {
				try {
					this.wait(TIMEOUT);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
				// invalidate sessions
				SessionFactory sf = (SessionFactory) ApplicationContextUtil.get("SessionFactorySingleton");
				Transaction tx = null;
				Session s = null;
				if (sf != null) {
					try {
						s = sf.openSession();
						tx = s.beginTransaction();
						String HQL = "FROM com.parking.entity.User WHERE current_timestamp() >= sessionexpiry";
						Query q = s.createQuery(HQL);
						ArrayList<User> users = (ArrayList<User>) q.list();
						users.forEach((user) -> {
							user.setSessionExpiry(null);
							user.setSessionID(null);
						});
					} catch (Exception e) {
						LogUtil.error(LogUtil.stackTraceToObject(e), e);
						tx.rollback();
					} finally {
						tx.commit();
						s.close();
					}
				}
			}
		}
	}

}
