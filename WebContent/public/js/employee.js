﻿function visualizeZoneValues() {
    $('.zoneNameValue').html(_zoneName);
    $('.zoneCapacityValue').html(_zoneCapacity);
    $('.zoneSpacesLeftValue').html(Number(_zoneCapacity) - Number(_zoneOccupied));
    $('.zonePriceValue').html(_zonePrice);
}

function mapAndControlsAnimation() {
    $('#googleMap').animate({ width: '500px' }, 500, function () {
        google.maps.event.trigger(_map, "resize");
        _map.setCenter(_map_center);
        showPrice();
        $('.info-table').fadeIn(400);
        $('.controls-box').fadeIn(400);
    });
}

function showPrice() {
    var price = Number($('.selectReg :selected').val()) * _zonePrice;
    var price = price.toFixed(2)

    $('.tax-box').html(price + " лв.");
    $('.selectReg').change(changePrice);
}

function changePrice() {
    var tax = Number($('.selectReg :selected').val()) * _zonePrice;
    var tax = tax.toFixed(2);
    $('.tax-box').html(tax + " лв.");
}

function onZoneClick() {
    mapAndControlsAnimation();

    $.ajax({
        type: 'POST',
        url: '/parking/rest/api/getzone',
        contentType: "application/json",
        data: '{"zoneId" : "' + this._polygon_zoneID + '"}',
        success: function (data, textStatus, xhr) {
            setZoneValues(data.zoneId, data.zoneName, data.zonePrice, data.zoneCapacity, data.zoneOccupied);
            visualizeZoneValues();
            showPrice();
        },
        error: function (xhr, error) {
            alert("Възникна грешка, моля опитайте по-късно");
        }
    });
}

$(document).ready(function () {
    $('#checkPlate').click(function () {
        var plate = $('#regNum').val()
        
        $.ajax({
            type: 'GET',
            url: '/parking/rest/api/checkplate?zoneId=' + _zoneID + '&plateNumber=' + plate,
            contentType: "application/json",
            success: function (data) {
                $('.response-msg-box').fadeOut(300, function () {
                    $('.response-msg-box').removeClass('error-response');
                    $('.response-msg-box').addClass('success-response').html("Кола с регистрационен номер " + plate + " е заплатила до " + data[data.length - 1].recordTo);
                    $('.response-msg-box').fadeIn(300);
                });
            },
            error: function (xhr) {
                console.log(xhr);
                $('.response-msg-box').fadeOut(300, function () {
                    $('.response-msg-box').removeClass('success-response');
                    if(xhr.status)
                        $('.response-msg-box').addClass('error-response').html("Кола с регистрационен номер " + plate + " не заплатила");
                    else
                        $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                    $('.response-msg-box').fadeIn(300);
                });
            }
        });
    });
});