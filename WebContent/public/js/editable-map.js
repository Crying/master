﻿var _map;
var _map_center;
var _markers = [];
var _dynamicPolygon;
var _polygons = [];

var _zoneName, _zonePrice, _zoneID, _zoneCapacity, _zoneOccupied;

$(document).ready(function () {
    $.get("/parking/rest/api/getallpolygons", function (data) {
        initialize(data);
    });
});

function visualizePolygons(zones) {
    for (var i = 0; i < zones.length; i++) {
        var polygonColor = '#0000FF';
        var coords = [];

        var lat = JSON.parse(zones[i].points.lat);
        var lon = JSON.parse(zones[i].points.lon);

        for (var j = 0; j < lat.length; j++) {
            coords.push({ lat: lat[j], lng: lon[j] });
        }

        var zone = new google.maps.Polygon({
            _polygon_zoneID: zones[i].zoneId,
            paths: coords,
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillColor: polygonColor,
            fillOpacity: 0.35
        });

        zone.setMap(_map);
        zone.addListener('click', onZoneClick);
    }
}

function initialize(zones) 
{
    var mapProp =
    {
        center: new google.maps.LatLng(42.696803, 23.320840),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDefaultUI: true
    };

    _map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    _map_center = _map.getCenter();
    google.maps.event.addListener(_map, "rightclick", rightClickEvent);

    visualizePolygons(zones);
}

function rightClickEvent(event)
{
    addMapMarker(event.latLng);

    //FIRA
    //if (checkIfIntersecsWithOtherPolygons(event.latLng.lat(), event.latLng.lng()))
    //{
    //    alert("Intersection with other polygon");
    //    _markers.pop();
    //}
    //else if(checkIfIntersectsWithSamePolygon())
    //{
    //      alert("complex polygon");
    //      _markers.pop();
    //}
    
    drawDynamiclyPolygon();
}

function addMapMarker(latLng)
{
    var marker = new google.maps.Marker({
        position: latLng,
        map: _map,
        draggable: true
    });

    _markers.push(marker);

    google.maps.event.addListener(marker, 'dragend', function (event) {
        drawDynamiclyPolygon();
    });
}

function drawDynamiclyPolygon()
{
    if(_dynamicPolygon)
    {
        _dynamicPolygon.setMap(null);
    }


    _dynamicPolygon = new google.maps.Polygon({
    paths: extractPositionsFromMarkets(),
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35
  });

    _dynamicPolygon.setMap(_map);
}

function extractPositionsFromMarkets()
{
    var positions = [];

    for (var i = 0; i < _markers.length; i++) {
        positions.push(_markers[i].position);
        console.log(_markers.position);
    }

    return positions;
}

function extractPositionsFromMarketsforApi()
{
    var positions = [];

    for (var i = 0; i < _markers.length; i++) {
        positions.push({lat: _markers[i].getPosition().lat(), lon: _markers[i].getPosition().lng()});
        console.log(_markers.position);
    }

    return positions;
}

//Fira
function checkIfIntersecsWithOtherPolygons(lat, lng) {
    for (var i = 0; i < _polygons.length; i++) {
        if (_polygons[i].containsLatLng(lat, lng)) {
            return true;
        }
    }

    return false;
}

//Fira
function checkIfIntersectsWithSamePolygon()
{
    var position = extractPositionsFromMarkets();

    if(position.length > 3)
    {
        for (var i = 0; i < position.length - extractPositionsFromMarkets(); i++) 
        {
            console.log(isIntersecting(position[position.length - 1], position[position.length - 2], position[i], position[i+1]));
            
            if(isIntersecting(position[position.length - 1], position[position.length - 2], position[i], position[i+1]))
                return true;
        }
    }

    return false;
}

//Fira
function ccw(a,b,c)
{
     return (c.lng-a.lng) * (b.lat-a.lat) > (b.lng-a.lng) * (c.lat-a.lat)
}

//Fira
// # Return true if line segments AB and CD intersect
function isIntersecting(a,b,c,d)
{
    return ccw(a,c,d) != ccw(b,c,d) && ccw(a,b,c) != ccw(a,b,d);
}

