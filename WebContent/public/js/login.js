﻿$(document).ready(function () { 
    $('#loginform').on('submit', function (e) {
        e.preventDefault();

        var credentials = {
            username: $('#username').val(),
            password: $('#password').val()
        }

        $.ajax({
            type: 'POST',
            url: '/parking/rest/api/login',
            contentType: "text/plain",
            data: JSON.stringify(credentials),
            success: function (data) {
                document.write(data);
                document.close();
            },
            error: function (xhr) {
                if(xhr.status == 403)
                {
                    alert("Грешен потребител или парола")
                }

                console.log(xhr.status);
            }
        });
    });
});

