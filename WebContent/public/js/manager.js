﻿$(document).ready(function () {
    $('.nav-tabs li').click(function () {
        $('.response-msg-box').hide();
    });

    $('#addZone').click(function () {
        var p = extractPositionsFromMarketsforApi();
        
        var json =
        {
            "zoneName": $('#zoneName').val(),
            "capacity": $('#zoneCapacity').val(),
            "price": $('#zonePrice').val(),
            "points": extractPositionsFromMarketsforApi()
        }

        if (zoneValidation(json)) {
            $.ajax({
                type: 'POST',
                url: '/parking/rest/api/addzone',
                contentType: "application/json",
                data: JSON.stringify(json),
                success: function (data) {
                    $('.response-msg-box').fadeOut(300, function () {
                        $('.response-msg-box').removeClass('error-response');
                        $('.response-msg-box').addClass('success-response').html("Зона '" + json.zoneName + "' беше добавeнa");
                        $('.response-msg-box').fadeIn(300);
                        $.get("/parking/rest/api/getallpolygons", function (data) {
                            initialize(data);
                        });
                    });
                },
                error: function (xhr) {
                    $('.response-msg-box').fadeOut(300, function () {
                        console.log(xhr.status)
                        $('.response-msg-box').removeClass('success-response');
                        $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                        $('.response-msg-box').fadeIn(300);
                    });
                }
            });
        }
    });

    $('#addUser').click(function () {
        usrname = $('#userName').val();

        if (userValidate(usrname)) {
            var json =
            {
                username: usrname,
                name: $('#emplName').val(),
                position: $('#selectPos :selected').val().toUpperCase(),
            }

            $.ajax({
                type: 'POST',
                url: '/parking/rest/api/addemployee',
                contentType: "application/json",
                data: JSON.stringify(json),
                success: function (data) {
                    $('.response-msg-box').fadeOut(300, function () {
                        $('.response-msg-box').removeClass('error-response');
                        $('.response-msg-box').addClass('success-response').html("Потребител '" + usrname + "' беше създаден. Парола: " + data.password);
                        $('.response-msg-box').fadeIn(300);
                    });
                },
                error: function (xhr) {
                    $('.response-msg-box').fadeOut(300, function () {
                        console.log(xhr.status)
                        $('.response-msg-box').removeClass('success-response');
                        $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                        $('.response-msg-box').fadeIn(300);
                    });
                }
            });
        }
    });

    $('#addUserToZone').click(function () {
        var json = {
            zoneId: _zoneID,
            employeeName: $('#useZoneName').val()
        }

        if (!_zoneID)
        {
            $('.response-msg-box').fadeOut(300, function () {
                $('.response-msg-box').removeClass('success-response');
                $('.response-msg-box').addClass('error-response').html("Моля изберете зона от картата");
                $('.response-msg-box').fadeIn(300);
            });

            return;
        }

        console.log(json);

        if (userValidate($('#useZoneName').val()))
        {
            $.ajax({
                type: 'POST',
                url: '/parking/rest/api/changeemployeezone',
                contentType: "text/plain",
                data: JSON.stringify(json),
                success: function (data) {
                    $('.response-msg-box').fadeOut(300, function () {
                        $('.response-msg-box').removeClass('error-response');
                        $('.response-msg-box').addClass('success-response').html("Потребител '" + json.employeeName + "' беше добавен към зона '" + _zoneName + "'");
                        $('.response-msg-box').fadeIn(300);
                    });
                },
                error: function (xhr) {
                    $('.response-msg-box').fadeOut(300, function () {
                        console.log(xhr.status)
                        $('.response-msg-box').removeClass('success-response');
                        $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                        $('.response-msg-box').fadeIn(300);
                    });
                }
            });
        }
    });

    $('#getStats').click(function () {

        var from = $('#fromYear :selected').val() + "-" + $('#fromMonth :selected').val() + "-" + $('#fromDay :selected').val() + "T" + $('#fromHours :selected').val() + ":" + $('#fromMinutes :selected').val() + ":00";
        var to = $('#toYear :selected').val() + "-" + $('#toMonth :selected').val() + "-" + $('#toDay :selected').val() + "T" + $('#toHours :selected').val() + ":" + $('#toMinutes :selected').val() + ":00";

        var json = {
            "zoneId": _zoneID,
            "startDate": from,
            "endDate": to
        }

        if (!_zoneID)
        {
            $('.response-msg-box').fadeOut(300, function () {
                $('.response-msg-box').removeClass('success-response');
                $('.response-msg-box').addClass('error-response').html("Моля изберете зона от картата");
                $('.response-msg-box').fadeIn(300);
            });

            return;
        }

        $.ajax({
            type: 'POST',
            url: '/parking/rest/api/statistics',
            contentType: "text/plain",
            data: JSON.stringify(json),
            success: function (data) {
                $('.response-msg-box').fadeOut(300, function () {
                    $('.response-msg-box').removeClass('error-response');
                    $('.response-msg-box').addClass('success-response').html("Общо паркирали автомобила: " + data.totalParked + ", и общо приходи: " + data.totalValue + "лв.");
                    $('.response-msg-box').fadeIn(300);
                });
            },
            error: function (xhr) {
                $('.response-msg-box').fadeOut(300, function () {
                    console.log(xhr.status)
                    $('.response-msg-box').removeClass('success-response');
                    $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                    $('.response-msg-box').fadeIn(300);
                });
            }
        });
    });

    dateTimeInputsInit();
});

function pad(num, size) {
    var s = num + "";
    
    while (s.length < size) 
    {
        s = "0" + s;
    }

    return s;
}

function dateTimeInputsInit()
{
    for (var i = 1; i < 32; i++) {
        $('#fromDay').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
        $('#toDay').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
    }

    for (var i = 1; i < 13; i++) {
        $('#fromMonth').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
        $('#toMonth').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
    }

    for (var i = 2015; i < 2031; i++) {
        $('#fromYear').append('<option value="' + i + '">' + i + '</option>');
        $('#toYear').append('<option value="' + i + '">' + i + '</option>');
    }

    for (var i = 0; i < 24; i++) {
        $('#fromHours').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
        $('#toHours').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
    }

    for (var i = 0; i < 60; i+= 15) {
        $('#fromMinutes').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
        $('#toMinutes').append('<option value="' + pad(i, 2) + '">' + pad(i, 2) + '</option>');
    }
}

function zoneValidation(zone)
{
    if (zone.points.length < 3) {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("За да добавите зона трябва да има най-малко 3 точки")
                .fadeIn(300);
        });

        return false;
    }

    if (!zone.zoneName) {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("За да добавите зона трябва да въведете име")
                .fadeIn(300);
        });

        return false;
    }


    if (isNaN(Number(zone.capacity)) || !zone.capacity) {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Невалиден капацитет, моля въведете числова стойност")
                .fadeIn(300);
        });

        return false;
    }
    else if (Number(zone.capacity) < 1 || Number(zone.capacity) > 1000)
    {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Капацитетът трябва да е цяло положително число по-голямо от 0 и по-малко от 1000")
                .fadeIn(300);
        });

        return false;
    }

    if (isNaN(Number(zone.price)) || !zone.price) {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Невалидна цена, моля въведете числова стойност")
                .fadeIn(300);
        });

        return false;
    }
    else if (Number(zone.price) < 0 || Number(zone.price) > 1000)
    {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Цената не може да е по-малка от 0.00 лв. или по-голяма от 1000.00 лв.")
                .fadeIn(300);
        });

        return false;
    }

    return true;
}

function userValidate(user) {
    if (!user) {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Невалидно потребителско име, моля въдете")
                .fadeIn(300);
        });

        return false;
    }

    console.log(user)
    var nameRegex = /^[a-zA-Z0-9]+$/;
    //var validfirstUsername = document.frm.firstName.value.match(nameRegex);
    var validUsername = nameRegex.test(user);

    if (validUsername == null)
    {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Невалидно потребителско име, само симфоли a-z A-Z и цифри")
                .fadeIn(300);
        });

        return false;
    }
    else if (user.length < 3 || user.length > 30)
    {
        $('.response-msg-box').fadeOut(300, function () {
            $('.response-msg-box')
                .removeClass('success-response')
                .addClass('error-response')
                .html("Невалидно потребителско име, името не трябва да бъде по-малко от 3 или по-дълго от 30 символа")
                .fadeIn(300);
        });

        return false;
    }

    return true;
}

function onZoneClick() {
    mapAndControlsAnimation();

    $.ajax({
        type: 'POST',
        url: '/parking/rest/api/getzone',
        contentType: "application/json",
        data: '{"zoneId" : "' + this._polygon_zoneID + '"}',
        success: function (data, textStatus, xhr) {
            setZoneValues(data.zoneId, data.zoneName, data.zonePrice, data.zoneCapacity, data.zoneOccupied);
            visualizeZoneValues();
        },
        error: function (xhr, error) {
            alert("Възникна грешка, моля опитайте по-късно");
        }
    });
}

function visualizeZoneValues() {
    $('.zoneNameValue').html(_zoneName);
    $('.zoneCapacityValue').html(_zoneCapacity);
    $('.zoneSpacesLeftValue').html(Number(_zoneCapacity) - Number(_zoneOccupied));
    $('.zonePriceValue').html(_zonePrice);
}

function mapAndControlsAnimation() {
    $('#googleMap').animate({ width: '500px' }, 500, function () {
        google.maps.event.trigger(_map, "resize");
        _map.setCenter(_map_center);
        $('.info-table').fadeIn(400);
    });
}

function setZoneValues(zoneID, zoneName, zonePrice, zoneCapacity, zoneOccupied) {
    _zoneID = zoneID;
    _zoneName = zoneName;
    _zonePrice = zonePrice;
    _zoneCapacity = zoneCapacity;
    _zoneOccupied = zoneOccupied;
}