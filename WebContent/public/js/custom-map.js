﻿var _map;
var _map_center;

var _zoneName, _zonePrice, _zoneID, _zoneCapacity, _zoneOccupied;

function initialize(zones) {
    var mapProp =
    {
        center: new google.maps.LatLng(42.696803, 23.320840),
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        disableDefaultUI: true
    };

    _map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
    _map_center = _map.getCenter();

    visualizePolygons(zones);
}

function visualizePolygons(zones) {
    for (var i = 0; i < zones.length; i++) {
        var polygonColor = '#0000FF';
        var coords = [];

        var lat = JSON.parse(zones[i].points.lat);
        var lon = JSON.parse(zones[i].points.lon);

        for (var j = 0; j < lat.length; j++) {
            coords.push({ lat: lat[j], lng: lon[j] });
        }

        var zone = new google.maps.Polygon({
            _polygon_zoneID: zones[i].zoneId,
            paths: coords,
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillColor: polygonColor,
            fillOpacity: 0.35
        });

        zone.setMap(_map);
        zone.addListener('click', onZoneClick);
    }
}

function setZoneValues(zoneID, zoneName, zonePrice, zoneCapacity, zoneOccupied) {
    _zoneID = zoneID;
    _zoneName = zoneName;
    _zonePrice = zonePrice;
    _zoneCapacity = zoneCapacity;
    _zoneOccupied = zoneOccupied;
}

function onZoneClick() {

}

$(document).ready(function () {
    $.get("/parking/rest/api/getallpolygons", function (data) {
        initialize(data);
    });
});