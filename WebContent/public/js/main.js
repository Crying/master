﻿function visualizeZoneValues() {
    $('.zoneNameValue').html(_zoneName);
    $('.zoneCapacityValue').html(_zoneCapacity);
    $('.zoneSpacesLeftValue').html(Number(_zoneCapacity) - Number(_zoneOccupied));
    $('.zonePriceValue').html(_zonePrice);
}

function mapAndControlsAnimation() {
    $('#googleMap').animate({ width: '500px' }, 500, function () {
        google.maps.event.trigger(_map, "resize");
        _map.setCenter(_map_center);
        $('.info-table').fadeIn(400);
        $('.controls-box').fadeIn(400);
    });
}

function showPrice() {
    var price = Number($('#time :selected').val()) * _zonePrice;
    var price = price.toFixed(2)

    $('.tax-box').html(price + " лв.");
    $('#time').change(changePrice);
}

function changePrice() {

    var tax = Number($('#time :selected').val()) * _zonePrice;
    var tax = tax.toFixed(2);
    $('.tax-box').html(tax + " лв.");
}

function onZoneClick() {
    mapAndControlsAnimation();

    $.ajax({
        type: 'POST',
        url: '/parking/rest/api/getzone',
        contentType: "application/json",
        data: '{"zoneId" : "' + this._polygon_zoneID + '"}',
        success: function (data, textStatus, xhr) {
            setZoneValues(data.zoneId, data.zoneName, data.zonePrice, data.zoneCapacity, data.zoneOccupied);
            visualizeZoneValues();
            showPrice();
        },
        error: function (xhr, error) {
            alert("Възникна грешка, моля опитайте по-късно");
        }
    });
}

$(document).ready(function () {
    $('#payBtn').click(function () {
        var jsonRecord = {
            time: Number($('#time :selected').val()),
            platenumber: $('#regNum').val(),
            zone: {
                zoneId: _zoneID
            }
        }

        $.ajax({
            type: 'POST',
            url: '/parking/rest/api/record',
            contentType: "application/json",
            data: JSON.stringify(jsonRecord),
            dataType: "text",
            mimeType: "text/plain",
            success: function () {
                $('.response-msg-box').fadeOut(300, function () {
                    $('.response-msg-box').removeClass('error-response');
                    $('.response-msg-box').addClass('success-response').html("Успешно заплащане");
                    $('.response-msg-box').fadeIn(300);
                });

                $.ajax({
                    type: 'POST',
                    url: '/parking/rest/api/getzone',
                    contentType: "application/json",
                    data: '{"zoneId" : "' + _zoneID + '"}',
                    success: function (data, textStatus, xhr) {
                        setZoneValues(data.zoneId, data.zoneName, data.zonePrice, data.zoneCapacity, data.zoneOccupied);
                        visualizeZoneValues();
                        showPrice();
                    },
                    error: function (xhr, error) {
                        alert("Възникна грешка, моля опитайте по-късно");
                    }
                });

            },
            error: function (xhr) {
                $('.response-msg-box').fadeOut(300, function () {
                    $('.response-msg-box').removeClass('success-response');

                    if (xhr.status === 403) {
                        $('.response-msg-box').addClass('error-response').html("Автомобил с регистрционен номер " + $('#regNum').val() + " вече е платил!");
                    }
                    else {
                        $('.response-msg-box').addClass('error-response').html("Възникна грешка!");
                    }

                    $('.response-msg-box').fadeIn(300);
                });

                $.ajax({
                    type: 'POST',
                    url: '/parking/rest/api/getzone',
                    contentType: "application/json",
                    data: '{"zoneId" : "' + _zoneID + '"}',
                    success: function (data, textStatus, xhr) {
                        setZoneValues(data.zoneId, data.zoneName, data.zonePrice, data.zoneCapacity, data.zoneOccupied);
                        visualizeZoneValues();
                        showPrice();
                    },
                    error: function (xhr, error) {
                        alert("Възникна грешка, моля опитайте по-късно");
                    }
                });
            }
        });

        
    });
});