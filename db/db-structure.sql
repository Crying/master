-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: parking
-- ------------------------------------------------------
-- Server version	5.6.26-enterprise-commercial-advanced-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `records`
--

DROP TABLE IF EXISTS `records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `records` (
  `recordid` bigint(64) NOT NULL AUTO_INCREMENT,
  `datefrom` datetime NOT NULL,
  `dateto` datetime NOT NULL,
  `paid` int(11) NOT NULL,
  `zoneid` bigint(64) NOT NULL,
  `platenumber` varchar(45) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  PRIMARY KEY (`recordid`),
  KEY `zoneid_fk3_idx` (`zoneid`),
  CONSTRAINT `zoneid_fk4` FOREIGN KEY (`zoneid`) REFERENCES `zones` (`zoneid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userid` bigint(64) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `position` varchar(45) NOT NULL,
  `hash` varchar(45) NOT NULL,
  `zoneid` bigint(64) DEFAULT NULL,
  `sessionid` varchar(60) DEFAULT NULL,
  `sessionexpiry` datetime DEFAULT NULL,
  PRIMARY KEY (`userid`),
  KEY `zoneid_fk2_idx` (`zoneid`),
  CONSTRAINT `zoneid_fk3` FOREIGN KEY (`zoneid`) REFERENCES `zones` (`zoneid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zonecoords`
--

DROP TABLE IF EXISTS `zonecoords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zonecoords` (
  `zoneid` bigint(64) NOT NULL,
  `pointlat` varchar(45) NOT NULL,
  `pointlon` varchar(45) NOT NULL,
  KEY `zoneid_fk1_idx` (`zoneid`),
  CONSTRAINT `zoneid_fk2` FOREIGN KEY (`zoneid`) REFERENCES `zones` (`zoneid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zoneemployee`
--

DROP TABLE IF EXISTS `zoneemployee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zoneemployee` (
  `userid` bigint(64) NOT NULL,
  `zoneid` bigint(64) NOT NULL,
  KEY `zoneid_idx` (`zoneid`),
  KEY `users_idx` (`userid`),
  CONSTRAINT `userid_fk1` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `zoneid_fk1` FOREIGN KEY (`zoneid`) REFERENCES `zones` (`zoneid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zones`
--

DROP TABLE IF EXISTS `zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zones` (
  `zoneid` bigint(64) NOT NULL AUTO_INCREMENT,
  `zonename` varchar(45) NOT NULL,
  `capacity` int(11) NOT NULL,
  `removed` tinyint(1) NOT NULL,
  `occupied` int(11) DEFAULT '0',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`zoneid`),
  UNIQUE KEY `zonename_UNIQUE` (`zonename`),
  UNIQUE KEY `zoneid_UNIQUE` (`zoneid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-20 19:02:14
